const bodyParser = require('body-parser');
const express = require('express');
const googlehome = require('google-home-notifier');

const app = express();

googlehome.device('Google Home'); 
googlehome.accent('uk');

const port = isNaN(parseInt(process.env.PORT)) ? 2300 : parseInt(process.env.PORT);

app.use(bodyParser.text({
    type: '*/*'
}));

app.post('/', (req, res) => {
    googlehome.notify(req.body, () => {
        res.status(200).send('OK');
    });
});

app.listen(port, () => {
    console.log(`Application is listening on ${port}`);
});
